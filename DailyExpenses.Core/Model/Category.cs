﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DailyExpenses.Core.Model
{
   public class Category:DataEntity
    {
        public string Name { get; set; }
        [ForeignKey("CreatedById")]
        public int CreatedById { get; set; }   
    }
}
