﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DailyExpenses.Core.Model
{
   public class User:DataEntity
    {
        public string FirstName { get; set; }
        public string LastName  { get; set; }
        public string Email { get; set; }
        public string Address { get; set; }
        public decimal Salary { get; set; }
    }
}
