﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DailyExpenses.Core.Model
{
   public class Expense:DataEntity
    {
        public decimal Amount { get; set; }
        public DateTime Date { get; set; }
        public int CreatedById { get; set; }
        [ForeignKey("CreatedById")]                                                                  
        public User CreatedBy { get; set; }
        public Heading Heading { get; set; }
    }
}
