﻿using DailyExpenses.Core.Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace DailyExpenses.Core.Interfaces
{
   public interface IGenericRepository<T> where T:DataEntity
    {
        T AddEntity(T entityToAdd);
        T GetentityById(int id);
        IEnumerable<T> GetAll();
        void DeleteEntity(T entityToDelete);
        T UpdateEntity(T entityToUpdate);
    }
}
