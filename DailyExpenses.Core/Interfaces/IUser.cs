﻿using DailyExpenses.Core.Model;

namespace DailyExpenses.Core.Interfaces
{
    public interface IUser
    {
       User UpdateUser(User usertoUpdate);
    }
}
