﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DailyExpenses.Core.Interfaces;
using DailyExpenses.Core.Model;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace DailyExpenses.WebAPI.Controllers
{
    [EnableCors("DailyExpensePolicy")]
    [Route("api/[controller]")]
    [ApiController]
    public class CategoryController : GenericRepositoryController<Category>
    {
        public CategoryController(IGenericRepository<Category> genericRepository) : base(genericRepository)
        { }

        [HttpPut]
        public ActionResult<Category> UpdateCatgory([FromBody]Category newCategory)
        {
            var result = GetEntityById(newCategory.Id);
            var oldCategory = ((ObjectResult)result.Result).Value as Category;
            oldCategory.Name = newCategory.Name;
            var updateCategory = UpdateEntity(oldCategory);
            return Ok(updateCategory);
        }
    }
}