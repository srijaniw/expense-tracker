﻿using DailyExpenses.Core.Interfaces;
using DailyExpenses.Core.Model;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;

namespace DailyExpenses.WebAPI.Controllers
{
    
    [Route("api/[controller]")]
    [ApiController]
    public class GenericRepositoryController<T> : ControllerBase where T : DataEntity
    {
        private readonly IGenericRepository<T> _genericRepository;
        public GenericRepositoryController(IGenericRepository<T> genericRepository)
        {
            _genericRepository = genericRepository;
        }

        [HttpPost("Add")]
        public ActionResult<T> AddEntity([FromBody] T entityToAdd)
        {
            try
            {
                var result = _genericRepository.AddEntity(entityToAdd);
                return Ok(result);
            }
            catch (System.Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        [HttpDelete("Delete/{id}")]
        public ActionResult DeleteEntity(int id)
        {
            try
            {
                var resultById = _genericRepository.GetentityById(id);
                _genericRepository.DeleteEntity(resultById);
                return Ok(new JsonResult("Delete Success"));
            }
            catch (System.Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        [HttpGet("GetAll")]
        public ActionResult<IEnumerable<T>> GetAll()
        {
            try
            {
                var result = _genericRepository.GetAll();
                return Ok(result);
            }
            catch (System.Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        [HttpGet("GetById/{id}")]
        public ActionResult<T> GetEntityById(int id)
        {
            try
            {
                var result = _genericRepository.GetentityById(id);
                return Ok(result);
            }
            catch (System.Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        [ApiExplorerSettings(IgnoreApi = true)]
        public T  UpdateEntity([FromBody] T entityToUpdate)
        {
           
            var entityFromDb = _genericRepository.UpdateEntity(entityToUpdate);
            return entityFromDb;
        }
    }
}