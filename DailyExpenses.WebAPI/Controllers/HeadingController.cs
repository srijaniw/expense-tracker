﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DailyExpenses.Core.Interfaces;
using DailyExpenses.Core.Model;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace DailyExpenses.WebAPI.Controllers
{
    [EnableCors("DailyExpensePolicy")]
    [Route("api/[controller]")]
    [ApiController]
    public class HeadingController : GenericRepositoryController<Heading>
    {
        public HeadingController(IGenericRepository<Heading> genericRepository) : base(genericRepository)
        { }
        public ActionResult<Heading> UpdateCatgory([FromBody]Heading newHeading)
        {
            var result = GetEntityById(newHeading.Id);
            var oldHeading = ((ObjectResult)result.Result).Value as Heading;
            oldHeading.Name = newHeading.Name;
            var updateHeading = UpdateEntity(oldHeading);
            return Ok(updateHeading);
        }
    }
}