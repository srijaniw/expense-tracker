﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DailyExpenses.Core.Interfaces;
using DailyExpenses.Core.Model;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace DailyExpenses.WebAPI.Controllers
{
    [EnableCors("DailyExpensePolicy")]
    [Route("api/[controller]")]
    [ApiController]
 
    public class ExpenseController : GenericRepositoryController<Expense>
    {
      public ExpenseController(IGenericRepository<Expense> genericRepository) : base(genericRepository)
      { }
    }
}