﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DailyExpenses.Core.Interfaces;
using DailyExpenses.Core.Model;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace DailyExpenses.WebAPI.Controllers
{
    [EnableCors("DailyExpensePolicy")]
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : GenericRepositoryController<User>
    {
        public UserController(IGenericRepository<User> genericRepository) : base(genericRepository)
        { }

        [HttpPut]
        public ActionResult<User> UpdateUser([FromBody]User newUser)
        {
            var result = GetEntityById(newUser.Id);
            var oldUser = ((ObjectResult)result.Result).Value as User;
            oldUser.FirstName = newUser.FirstName;
            oldUser.LastName = newUser.LastName;
            oldUser.Address = newUser.Address;
            oldUser.Email = newUser.Email;
            oldUser.Salary = newUser.Salary;
            var updateUser = UpdateEntity(oldUser);
            return Ok(updateUser);
        }
    }
}