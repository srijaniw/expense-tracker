import { CommonModule, LocationStrategy, PathLocationStrategy } from '@angular/common';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AuthGuard } from './shared';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FileDropDirective } from './_directives/file-drop.directive';

import { AngularFireModule } from 'angularfire2';
import { AngularFirestoreModule } from 'angularfire2/firestore';
import { AngularFireDatabase } from 'angularfire2/database';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { ToastrModule } from 'ngx-toastr';

import { SweetAlert2Module } from '@toverux/ngx-sweetalert2';
import { UserService } from './_services/user.service';
import { CategoryService } from './_services/category.service';
import { ExpenseService } from './_services/expense.service';
import { HeadingService } from './_services/heading.service';



export const createTranslateLoader = (http: HttpClient) => {
    return new TranslateHttpLoader(http, './assets/i18n/', '.json');
};

@NgModule({
    imports: [
        CommonModule,
        BrowserModule,
        BrowserAnimationsModule,
        HttpClientModule,
        HttpModule,
        FormsModule,
        TranslateModule.forRoot({
            loader: {
                provide: TranslateLoader,
                useFactory: createTranslateLoader,
                deps: [HttpClient]
            }
        }),
        NgbModule,
        AngularFireModule,
        AngularFirestoreModule,
        AppRoutingModule,
        FormsModule,
        ReactiveFormsModule,
        ToastrModule.forRoot(),
        SweetAlert2Module.forRoot(),

    ],
    declarations:
        [
            AppComponent,
            FileDropDirective
        ],
    providers:
        [
            AuthGuard,
            AngularFireDatabase,
            HttpModule,
            HttpClientModule,
            { provide: LocationStrategy, useClass: PathLocationStrategy },
            UserService,
            CategoryService,
            ExpenseService,
            HeadingService,
        ],
    bootstrap:
        [
            AppComponent
        ]
})
export class AppModule { }
