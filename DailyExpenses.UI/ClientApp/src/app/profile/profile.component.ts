import { Component, OnInit } from '@angular/core';
import { UserService } from '../_services/user.service';
import { User } from '../models/user.model';
import { computeStyle } from '@angular/animations/browser/src/util';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {
    public users: User = new User();
    public userList: User = new User();
  constructor(private userService: UserService) { }

  ngOnInit() {
    this.userService.GetById(1).subscribe((data) => {
        this.userList = data;
        this.users = data;
      });
   }

updateUser(user) {
console.log(user);
this.userService.UpdateUser(user).subscribe((data) => {
    alert('User Updated');
});
}
}
