import { Injectable } from '@angular/core';
import { Heading } from '../models/heading.model';
import { GenericService } from './generic.service';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';

@Injectable()
export class HeadingService extends GenericService<Heading> {

  constructor(httpClient: HttpClient)  {
    super(
      httpClient,
      environment.apiurl,
      'heading'
    );
   }
   public UpdateHeading(update) {
    return this.httpClient.put(`${environment.apiurl}/Update`, update);
   }

}
