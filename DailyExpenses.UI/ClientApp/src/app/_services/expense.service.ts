import { Injectable } from '@angular/core';
import { GenericService } from './generic.service';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { Expense } from '../models/expense.model';

@Injectable()
export class ExpenseService extends GenericService<Expense> {

  constructor(httpClient:HttpClient)  {
    super(
      httpClient,
      environment.apiurl,
      'expense'
    );
   }
   public UpdateExpense(expense) {
    return this.httpClient.put(`${environment.apiurl}/Expense`, expense);
   }
}
