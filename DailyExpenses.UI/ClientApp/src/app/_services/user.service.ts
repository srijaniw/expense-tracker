import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { GenericService } from './generic.service';
import { User } from '../models/user.model';

@Injectable()
export class UserService extends GenericService<User> {

  constructor(httpClient: HttpClient)  {
    super(
      httpClient,
      environment.apiurl,
      'user'
    );
   }

   public UpdateUser(user) {
    return this.httpClient.put(`${environment.apiurl}/User`, user);
   }
}
