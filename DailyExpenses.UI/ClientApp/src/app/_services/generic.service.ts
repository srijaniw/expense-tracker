import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map, catchError } from 'rxjs/operators'
import { DataEntity } from '../models/dataEntity.model';

export class GenericService<T extends DataEntity> {
  private _token: string = localStorage.getItem('auth_token');

  constructor(
    protected httpClient: HttpClient,
    private apiUrl: string,
    private endPoint: string
  ) { }

  protected getCommonOptions() {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };
    return httpOptions;
  }
  private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', error.error.message);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      console.error(
        `Backend returned code ${error.status}, ` +
        `body was: ${error.error}`);
    }
  }

  public AddEntity(entity: T): Observable<T> {
    return this.httpClient.post<T>(`${this.apiUrl}/${this.endPoint}/Add`, entity, this.getCommonOptions());
  }

//   public UpdateEntity(entity: T): Observable<T> {
//     return this.httpClient.put<T>(`${this.apiUrl}/${this.endPoint}/Update`, entity, this.getCommonOptions());
//   }

  public GetById(id: number): Observable<T> {
    return this.httpClient.get<T>(`${this.apiUrl}/${this.endPoint}/GetById/${id}`);
  }

  public GetAll(): Observable<T[]> {
    return this.httpClient.get<T[]>(`${this.apiUrl}/${this.endPoint}/GetAll`);
  }

  public DeleteEntity(id: number): Observable<any> {
    return this.httpClient.delete<any>(`${this.apiUrl}/${this.endPoint}/Delete/${id}`);
  }
}
