import { Injectable } from '@angular/core';
import { GenericService } from './generic.service';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { Category } from '../models/category.model';

@Injectable()
export class CategoryService extends GenericService<Category> {

  constructor(httpClient: HttpClient)  {
    super(
      httpClient,
      environment.apiurl,
      'category'
    );
   }
   public UpdateCatgory(category) {
    return this.httpClient.put(`${environment.apiurl}/Category`, category);
   }
}
