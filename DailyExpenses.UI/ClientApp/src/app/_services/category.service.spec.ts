import { TestBed, inject, getTestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { environment } from '../../environments/environment';
import { CategoryService } from './category.service';

fdescribe('CategoryService', () => {
 let httpMock: HttpTestingController;
  let injector: TestBed;
  let service: CategoryService;
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CategoryService]
    });
    injector = getTestBed();
    httpMock = injector.get(HttpTestingController);
    service = injector.get(CategoryService);
  });

//   afterEach(() => {
//     httpMock.verify();
//   });

  it('should get all category data', inject( [HttpTestingController, CategoryService],
    ( httpMock: HttpTestingController, service: CategoryService) => {
        const mockResponse = {
          'id': 2,
          'name': 'Groceries',
          'createdBy': 1
        };
        service.GetAll().subscribe(fakeData => {
            expect(fakeData.length).toBe(1);
            expect(fakeData[0].name).toEqual('Groceries');
        });
        const req = httpMock.expectOne(`${environment.apiurl}/Category/GetAll`);
        expect(req.request.method).toEqual('GET');
        req.flush(mockResponse, { status: 200, statusText: 'Ok' });
  }));

  fit('should add category data', inject( [HttpTestingController, CategoryService],
    ( httpMock: HttpTestingController, service: CategoryService) => {
        const mockResponse = {
            'id': 2,
            'name': 'Groceries',
            'createdBy': 1
          };
        service.AddEntity(mockResponse).subscribe(fakeData => {
            expect(fakeData[0].name).toEqual('Groceries');
        });
        const req = httpMock.expectOne(`${environment.apiurl}/Category/Add`);
        expect(req.request.method).toEqual('POST');
        req.flush(mockResponse, { status: 200, statusText: 'Ok' });
  }));

  it('should delete category data', inject( [HttpTestingController, CategoryService],
    ( httpMock: HttpTestingController, service: CategoryService) => {
        const mockResponse = {
            'id': 2,
            'name': 'Groceries',
            'createdBy': 1
          };
        service.DeleteEntity(mockResponse.id).subscribe(fakeData => {
        });
        const req = httpMock.expectOne(`${environment.apiurl}/Category/Delete/2`);
        expect(req.request.method).toEqual('DELETE');
        req.flush(mockResponse, { status: 200, statusText: 'Ok' });
  }));

  it('should get all category data by Id', inject( [HttpTestingController, CategoryService],
    ( httpMock: HttpTestingController, service: CategoryService) => {
        const mockResponse = {
            'id': 2,
            'name': 'Groceries',
            'createdBy': 1
          };
        service.GetById(2).subscribe(fakeData => {
            expect(fakeData[0].name).toEqual('Groceries');
        });
        const req = httpMock.expectOne(`${environment.apiurl}/Category/GetById/2`);
        expect(req.request.method).toEqual('GET');
        req.flush(mockResponse, { status: 200, statusText: 'Ok' });
  }));
});
