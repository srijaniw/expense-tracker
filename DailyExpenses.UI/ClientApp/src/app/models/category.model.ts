import { DataEntity } from "./dataEntity.model";
import { User } from "./user.model";

export class Category extends DataEntity{
    name: string;
    createdBy: number;
}
