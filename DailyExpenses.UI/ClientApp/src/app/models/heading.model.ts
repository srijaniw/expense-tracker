import { DataEntity } from "./dataEntity.model";
import { Category } from "./category.model";
import { Expense } from "./expense.model";
import { User } from "./user.model";

export class Heading extends DataEntity{
name:string;
category:Category;
user:User;
}
