import { User } from "./user.model";
import { DataEntity } from "./dataEntity.model";
import { Heading } from "./heading.model";

export class Expense extends DataEntity{
    amount:string;
    date:Date;
    user:User;
    heading:Heading;

}
