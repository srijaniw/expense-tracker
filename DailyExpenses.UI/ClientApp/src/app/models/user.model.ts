import { DataEntity } from "./dataEntity.model";

export class User extends DataEntity{
 firstName:string;
 lastName:string;
 email:string;
 address:string;
 salary:string;   
}