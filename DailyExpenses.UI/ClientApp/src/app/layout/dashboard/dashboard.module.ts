import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgbCarouselModule, NgbAlertModule, NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { DashboardRoutingModule } from './dashboard-routing.module';
import { DashboardComponent } from './dashboard.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TagInputModule } from 'ngx-chips';

import { SweetAlert2Module } from '@toverux/ngx-sweetalert2';
import { FulfillingBouncingCircleSpinnerModule } from 'angular-epic-spinners'
import { HttpModule } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';
import { StatModule } from '../../shared';
import { ProfileComponent } from '../../profile/profile.component';
import { ExpensesComponent } from '../../expenses/expenses.component';
import { CategoriesComponent } from '../../categories/categories.component';
import { HttpTestingController } from '@angular/common/http/testing';

@NgModule({
    imports: [
        CommonModule,
        NgbCarouselModule.forRoot(),
        NgbAlertModule.forRoot(),
        DashboardRoutingModule,
        StatModule,
        TagInputModule,
        NgbModule,
        HttpClientModule,
        FormsModule,
        HttpModule,
        ReactiveFormsModule,
        SweetAlert2Module.forRoot(),
        FulfillingBouncingCircleSpinnerModule

    ],
    declarations: [
        DashboardComponent,
        ProfileComponent,
        ExpensesComponent,
        CategoriesComponent

    ],
    providers: [HttpClientModule]
})
export class DashboardModule { }
