import { NgModule, Component } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DashboardComponent } from './dashboard.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ProfileComponent } from '../../profile/profile.component';
import { CategoriesComponent } from '../../categories/categories.component';
import { ExpensesComponent } from '../../expenses/expenses.component';


const routes: Routes = [
    { path: '', component: DashboardComponent },
    // { path: 'modules', component: ModulesComponent },
    { path: 'profile', component: ProfileComponent },
    {path: 'categories', component: CategoriesComponent},
    {path: 'expenses', component : ExpensesComponent}
];

@NgModule({
    imports: [NgbModule.forRoot(), RouterModule.forChild(routes)],
    declarations: [],
    exports: [RouterModule]
})
export class DashboardRoutingModule {
}
