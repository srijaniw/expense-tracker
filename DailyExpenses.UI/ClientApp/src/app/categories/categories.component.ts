import { Component, OnInit } from '@angular/core';
import { Category } from '../models/category.model';
import { CategoryService } from '../_services/category.service';
import { HeadingService } from '../_services/heading.service';
import { Heading } from '../models/heading.model';

@Component({
    selector: 'app-categories',
    templateUrl: './categories.component.html',
    styleUrls: ['./categories.component.scss']
})
export class CategoriesComponent implements OnInit {
    public categories: Category = new Category();
    public categoryList: Category[];
    public headings: Heading = new Heading();
    public headingList: Heading[];
    dropdownValue: number;
    cId: number;
    constructor(
        private categoryService: CategoryService,
        private headingService: HeadingService
    ) {}

    ngOnInit() {
        this.getHeading();
        this.getCategory();
    }
    getCategory() {
        this.categoryService.GetAll().subscribe(data => {
            this.categoryList = data;
        });
    }

    getHeading() {
        this.headingService.GetAll().subscribe((data) => {
            this.headingList = data;
        });
    }
    onCSubmit(categories) {
        if (categories.id == null) {
            this.categoryService.AddEntity(categories).subscribe((data) => {
                this.getCategory();
                alert('Categories Added');
            });
        } else {
            this.categoryService.UpdateCatgory(categories).subscribe((data) => {
                this.getCategory();
                alert('Categories Updated');
            });
        }
    }
    onHSubmit(headings) {
        if (headings.id == null) {
            console.log(headings);
            this.headingService.AddEntity(headings).subscribe((data) => {
                this.getHeading();
                alert('Headings Added');
            });
        } else {
            this.headingService.UpdateHeading(headings).subscribe((data) => {
                this.getHeading();
                alert('Headings Updated');
            });
        }

    }

    onDeleteCategory(categoryId) {
        this.categoryService.DeleteEntity(categoryId).subscribe((data) => {
            this.getCategory();
            alert('Category Deleted');
        });
    }

    onDeleteHeading(headingId) {
        this.headingService.DeleteEntity(headingId).subscribe((data) => {
            this.getHeading();
            alert('Heading Deleted');
        });
    }

    onCategoryEdit(category) {
        this.categories.id = category.id;
        this.categories.name = category.name;
    }
    onHeadingEdit(heading) {
        this.headings.id = heading.id;
        this.headings.name = heading.name;
        console.log(this.headings);

    }

    selectCategory(event: any) {
        // tslint:disable-next-line:radix
         this.cId = parseInt(event.target.value);
         this.dropdownValue = this.cId;
    }
}
