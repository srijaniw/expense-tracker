﻿using DailyExpenses.Core.Model;
using Microsoft.EntityFrameworkCore;

namespace DailyExpenses.Infrastructure
{
    public class DailyExpensesContext:DbContext
    {
        public DailyExpensesContext(DbContextOptions<DailyExpensesContext> options) : base(options)
        {}
        public virtual DbSet<User> Users { get; set; }
        public virtual DbSet<Expense> Expenses { get; set; }
        public virtual DbSet<Heading> Headings { get; set; }
        public virtual DbSet<Category> Categories { get; set; }

    }
}
