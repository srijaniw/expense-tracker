﻿using DailyExpenses.Core.Interfaces;
using DailyExpenses.Core.Model;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace DailyExpenses.Infrastructure.Implementation
{
    public class GenericRepository<TEntity> : IGenericRepository<TEntity> where TEntity : DataEntity
    {
        private readonly DailyExpensesContext _context;
        private readonly DbSet<TEntity> _dbSet;

        public GenericRepository(DailyExpensesContext context)
        {
            _context = context ?? throw new ArgumentNullException("Context was not supplied");
            _dbSet = _context.Set<TEntity>();
        }
        public TEntity AddEntity(TEntity entityToAdd)
        {
            _dbSet.Add(entityToAdd);
            Save();
            return entityToAdd;
        }

        public void DeleteEntity(TEntity entityToDelete)
        {
            _dbSet.Attach(entityToDelete);
            _dbSet.Remove(entityToDelete);
            Save();
        }

        public IEnumerable<TEntity> GetAll()
        {
            try
            {
                return _dbSet.AsEnumerable();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
           
        }

        private IEnumerable<TEntity> BadRequest(object message)
        {
            throw new NotImplementedException();
        }

        public TEntity GetentityById(int id)
        {
           return _dbSet.Find(id);
        }

        public TEntity UpdateEntity(TEntity entityToUpdate)
        {
            _dbSet.Attach(entityToUpdate);
            _context.Entry(entityToUpdate).State = EntityState.Modified;
            Save();
            return entityToUpdate;
        }
        public void Save() => _context.SaveChanges();
    }
}
