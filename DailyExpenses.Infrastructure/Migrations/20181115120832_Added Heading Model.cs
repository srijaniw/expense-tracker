﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DailyExpenses.Infrastructure.Migrations
{
    public partial class AddedHeadingModel : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Headings_Categories_CreatedById",
                table: "Headings");

            migrationBuilder.DropForeignKey(
                name: "FK_Headings_Users_CreatedById1",
                table: "Headings");

            migrationBuilder.DropIndex(
                name: "IX_Headings_CreatedById1",
                table: "Headings");

            migrationBuilder.DropColumn(
                name: "CreatedById1",
                table: "Headings");

            migrationBuilder.CreateIndex(
                name: "IX_Headings_CategoryId",
                table: "Headings",
                column: "CategoryId");

            migrationBuilder.AddForeignKey(
                name: "FK_Headings_Categories_CategoryId",
                table: "Headings",
                column: "CategoryId",
                principalTable: "Categories",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Headings_Users_CreatedById",
                table: "Headings",
                column: "CreatedById",
                principalTable: "Users",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Headings_Categories_CategoryId",
                table: "Headings");

            migrationBuilder.DropForeignKey(
                name: "FK_Headings_Users_CreatedById",
                table: "Headings");

            migrationBuilder.DropIndex(
                name: "IX_Headings_CategoryId",
                table: "Headings");

            migrationBuilder.AddColumn<int>(
                name: "CreatedById1",
                table: "Headings",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Headings_CreatedById1",
                table: "Headings",
                column: "CreatedById1");

            migrationBuilder.AddForeignKey(
                name: "FK_Headings_Categories_CreatedById",
                table: "Headings",
                column: "CreatedById",
                principalTable: "Categories",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Headings_Users_CreatedById1",
                table: "Headings",
                column: "CreatedById1",
                principalTable: "Users",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
