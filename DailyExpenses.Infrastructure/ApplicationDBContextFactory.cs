﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using System.Reflection;

namespace DailyExpenses.Infrastructure
{
    public class ApplicationDBContextFactory : IDesignTimeDbContextFactory<DailyExpensesContext>
    {
        public DailyExpensesContext CreateDbContext(string[] args)
        {
            var builder = new DbContextOptionsBuilder<DailyExpensesContext>();
            builder.UseSqlServer("Server=.; Database=DailyExpense;Trusted_Connection=True",
                optionsBUilder=>optionsBUilder.MigrationsAssembly(typeof(DailyExpensesContext).GetTypeInfo().Assembly.GetName().Name));
            return new DailyExpensesContext(builder.Options);
        }
    }
}
