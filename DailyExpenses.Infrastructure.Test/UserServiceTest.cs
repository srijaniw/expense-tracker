﻿using DailyExpenses.Core.Model;
using DailyExpenses.Infrastructure.Implementation;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using Xunit;

namespace DailyExpenses.Infrastructure.Test
{
    public class UserServiceTest
    {
        private readonly DbContextOptions<DailyExpensesContext> _fakeDbContext;
        private readonly User _user;
        private GenericRepository<User> _genericRepo = null;
        public UserServiceTest()
        {
            _fakeDbContext = new DbContextOptionsBuilder<DailyExpensesContext>().UseInMemoryDatabase(databaseName: "UsingTestDatabase").Options;

            _user = new User
            {
                Id = 1,
                FirstName = "Srijan",
                LastName = "Shrestha",
                Email = "srijan@gmail.com",
                Address = "Satdobato"

            };
        }

        [Fact]
        public void GetAllUserTest_Shoould_GetAllUser_From_UserService()
        {
            using (var context = new DailyExpensesContext(_fakeDbContext))
            {
                //ARRANGE

                context.AddRange(_user);
                context.SaveChanges();

                //Act
                _genericRepo = new GenericRepository<User>(context);
                var result = _genericRepo.GetAll().ToList();

                //Assert
                Assert.Equal("Srijan", result[0].FirstName);
                Assert.Equal("Shrestha", result[0].LastName);
            }
        }

        [Fact]
        public void GetUserbyIdTest_Shoould_GetUser_From_UserService()
        {
            using (var context = new DailyExpensesContext(_fakeDbContext))
            {
                //ARRANGE
                context.Database.EnsureDeleted();
                context.AddRange(_user);
                context.SaveChanges();

                //Act
                _genericRepo = new GenericRepository<User>(context);
                var result = _genericRepo.GetentityById(1);

                //Assert
                Assert.Equal("Srijan", result.FirstName);
                Assert.Equal("Shrestha", result.LastName);
            }
        }

        [Fact]
        public void AddUserTest_should_AddUser()
        {
            using (var context = new DailyExpensesContext(_fakeDbContext))
            {
                //ARRANGE
                context.Database.EnsureDeleted();

                //Act
                _genericRepo = new GenericRepository<User>(context);
                var result = _genericRepo.AddEntity(_user);
                var data = _genericRepo.GetAll().ToList();

                //Assert
                Assert.Equal("Srijan", data[0].FirstName);
                Assert.Equal("Shrestha", data[0].LastName);
            }
        }

        [Fact]
        public void UpdateUserTest_should_UpdateUser()
        {
            using (var context = new DailyExpensesContext(_fakeDbContext))
            {
                //ARRANGE
                context.Database.EnsureDeleted();
                context.AddRange(_user);
                context.SaveChanges();

                //Act
                _user.Id = 1;
                _user.FirstName = "Ram";
                _user.LastName = "Shrestha";
                _user.Email = "srijan@gmail.com";
                _user.Address = "Satdobato";

               // _genericRepo = new GenericRepository<User>(context);
                var result = _genericRepo.UpdateEntity(_user);
                var data = _genericRepo.GetAll().ToList();

                //Assert
                Assert.Equal("Ram", data[0].FirstName);
            }
        }

        //[Fact]
        //public void DeleteUserTest_should_DeleteUser()
        //{
        //    using (var context = new DailyExpensesContext(_fakeDbContext))
        //    {
        //        //ARRANGE
        //        context.AddRange(_user);
        //        context.SaveChanges();
        //        //Act
        //        _genericRepo = new GenericRepository<User>(context);
        //         _genericRepo.DeleteEntity(_user);
        //        var result = _genericRepo.GetentityById(_user.Id);

        //        //Assert
        //        var ex = Assert.Throws<ArgumentNullException>(() =>
        //       Assert.Null(result.FirstName));
        //        Assert.Equal("Object reference not set to an instance of an object.\r\nParameter name: template", ex.Message);
        //    }
        //}

    }
}